#The first thing to do is to clone the repository:
git clone https://gitlab.com/Dzhanbekov/comments.git

#create virtualenvirement:
python3 -m venv env

#for mac:
virtualenv env -p python3

#activation virtualenvirement:
source env/bin/activate

#Then install the dependencies:
pip install -r requirements.txt

#make all migrations:
python manage.py makemigrations
python manage.py migrate

#Then Run local server:
python manage.py runserver

#Swagger url:
url = http://localhost:8000/api/v1/doc/