from drf_spectacular import openapi
from rest_framework import serializers
from .models import Post, CommentPost


class FilterCommentSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        data = data.filter(parent=None)
        return super().to_representation(data)


class RecursiveSerializer(serializers.Serializer):

    def to_representation(self, instance):
        serializer = self.parent.parent.__class__(instance, context=self.context)
        return serializer.data


class PostCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = '__all__'

    def create(self, validated_data):
        return Post.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.text = validated_data.get('text', instance.text)
        instance.save()
        return instance


class CommentCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = CommentPost
        fields = '__all__'

    def create(self, validated_data):
        return CommentPost.objects.create(**validated_data)


class CommentRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = CommentPost
        fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):
    children = RecursiveSerializer(many=True)

    class Meta:
        list_serializer_class = FilterCommentSerializer
        model = CommentPost
        fields = ('name', 'text', 'children')


class PostDetailSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True)

    class Meta:
        model = Post
        fields = ('title', 'text', 'date', 'config')
