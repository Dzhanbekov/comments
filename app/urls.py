from django.urls import path
from . import views

urlpatterns = [
    path('post/', views.PostCreateListView.as_view()),
    path('post/detail/<int:pk>/', views.PostDetailView.as_view()),
    path('comment/create/', views.CommentCreateView.as_view()),
    path('comment/list/', views.CommentListView.as_view()),
    path('comment/<int:pk>/', views.CommentRetrieveView.as_view()),
]
