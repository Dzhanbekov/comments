from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=150, verbose_name='Описапие')
    text = models.TextField(verbose_name='текст статьи')
    date = models.DateTimeField(auto_now_add=True, verbose_name='дата публикации')

    def __str__(self):
        return self.title


class CommentPost(models.Model):
    name = models.CharField("Имя", max_length=100)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name='публикация', related_name='config')
    text = models.TextField(verbose_name='текст пользователя')
    parent = models.ForeignKey(
        'self', verbose_name='родитель', on_delete=models.SET_NULL, blank=True, null=True, related_name='children'
    )
    created = models.DateTimeField(auto_now_add=True, verbose_name='добавлен')

    def __str__(self):
        return f'{self.name} - {self.post}'

