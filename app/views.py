from django.shortcuts import render, get_object_or_404
from drf_spectacular import openapi
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveDestroyAPIView
from rest_framework.response import Response

from .models import Post, CommentPost
from .serializers import CommentCreateSerializer, PostCreateSerializer, PostDetailSerializer, \
    CommentSerializer, CommentRetrieveSerializer
from rest_framework.views import APIView


class PostDetailView(APIView):

    def get(self, request, pk):
        post = get_object_or_404(Post, pk=pk)
        serializer = PostDetailSerializer(post)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        post = get_object_or_404(Post, pk=pk)
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pk, format=None):
        post = get_object_or_404(Post, pk=pk)
        serializer = PostCreateSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        post = get_object_or_404(Post, pk=pk)
        serializer = PostCreateSerializer(post, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class PostCreateListView(APIView):
    serializer_class = PostCreateSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get(self, request, format=None):
        post = Post.objects.all().order_by('-id')
        serializer = self.serializer_class(post, many=True)
        return Response(serializer.data)


class CommentCreateView(CreateAPIView):
    queryset = CommentPost.objects.all()
    serializer_class = CommentCreateSerializer


class CommentListView(ListAPIView):
    queryset = CommentPost.objects.all()
    serializer_class = CommentSerializer


class CommentRetrieveView(RetrieveDestroyAPIView):
    queryset = CommentPost.objects.all()
    serializer_class = CommentRetrieveSerializer


